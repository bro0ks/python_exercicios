a = {}

for i in range(3):
    key = input("Insira o seu nome: ")
    value = input("Insira a sua idade: ")
    a[key] = value

print(f'O mais velho é o {max(a, key=a.get)}')
print(f'O mais novo é o {min(a, key=a.get)}')