#Crie uma calculadora para o IMC(peso/ (altura x altura)) e indique o estado da pessoa dada à seguinte tabela:
    #Magreza, quando o resultado é menor que 18,5 kg/m2;
    #Normal, quando o resultado está entre 18,5 e 24,9 kg/m2;
    #Sobrepeso, quando o resultado está entre 24,9 e 30 kg/m2;
    #Obesidade, quando o resultado é maior que 30 kg/m2.

print('***************************')
print('***** Calculadora IMC *****')
print('***************************')

print('Digite o seu peso: ')
a = float(input())
print('Digite a sua altura: ')
b = float(input())

c = a / (b * b)

if c < 18.5:
     print(f'Magreza, {c}')
elif c >= 18.5 and c <= 24.9:
     print(f'Normal, {c}')
elif c >= 24.9 and c <= 30.0:
     print(f'Sobrepeso, {c}')
elif c > 30.0:
     print(f'Obesidade, {c}')